import { Component, OnInit,ElementRef  } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  constructor(private el: ElementRef) {
}

  ngOnInit(): void {
  }

 toggleNavbar(){
  let myTag = this.el.nativeElement.querySelector(".navbar-collapse");
  myTag.classList.remove('show');
 }
}
