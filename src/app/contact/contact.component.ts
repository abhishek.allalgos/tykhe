import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewEncapsulation,ElementRef } from '@angular/core';
import { ReactiveFormsModule,FormsModule,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactHTTPService } from './contact.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {


  defaultAuth: any = {
      name:"",
      email:"",
      msg:""
  };

   contactForm: FormGroup;
   hasError: boolean;


  constructor( private fb: FormBuilder,
    private route: ActivatedRoute,
    private el: ElementRef,
    private contactserv: ContactHTTPService
    ) {

     }

   ngOnInit(): void {
            setTimeout(() => 
{
    let myTag = document.getElementsByClassName("fade-up-scroll");
    myTag[0].setAttribute('data-aos','');
    
},
500);

            setTimeout(() => 
{
    let myTag = document.getElementsByClassName("fade-down-scroll");
    myTag[0].setAttribute('data-aos','');
    
},
1000);
    this.initForm();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.contactForm.controls;
  }

    initForm() {
    this.contactForm = this.fb.group({
      name: [
        this.defaultAuth.name,
        Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(320), 
        ]),
      ],
      email: [
        this.defaultAuth.email,
        Validators.compose([
          Validators.required,
          Validators.email,
          Validators.minLength(6),
          Validators.maxLength(320), 
        ]),
      ],
      msg: [
        this.defaultAuth.msg,
        Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(320),
        ]),
      ],
    });
  }


     submit() {
 
    this.hasError = false;

    console.log("hhhhhhhh",this.contactForm.value);

 

    this.contactserv.sendMail(this.contactForm.value.name,this.contactForm.value.msg,this.contactForm.value.email).subscribe((event: any=[]) => {
         this.contactForm.reset();
    alert("Mail Sent Successfully.");

      console.log("sss",event);
    });

  }


}
