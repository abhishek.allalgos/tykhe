import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ContactHTTPService {

  constructor(private http: HttpClient) { }


  sendMail(name:any,msg:any,email:any): Observable<any> {
    //console.log("log",data);
    return this.http.get(`https://be607f190c6941317.temporary.link/mail.php?name=${name}&email=${email}&msg=${msg}`);
  }

}