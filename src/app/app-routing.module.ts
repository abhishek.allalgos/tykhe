import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LayoutComponent} from './_layouts/layout/layout.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { TeamComponent } from './team/team.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  {    path: '',
         component: LayoutComponent, 
        children: [
          { path: '', component: HomeComponent , pathMatch:'full' },
          { path: 'home', component: HomeComponent },
          { path: 'portfolio', component: PortfolioComponent },
          { path: 'team', component: TeamComponent },
          { path: 'contact', component: ContactComponent },
        ]

   },

];

// const routes: Routes = [

//   {    path: '',
//          component: LayoutComponent, 
//          children: [
//         {
//           path: 'home',
//           loadChildren: () => import('./home/home.component').then(m => m.HomeComponent)
//         },
//         {
//           path: 'portfolio',
//           loadChildren: () => import('./portfolio/portfolio.component').then(m => m.PortfolioComponent)
//         },
//         {
//           path: 'team',
//           loadChildren: () => import('./team/team.component').then(m => m.TeamComponent)
//         },
//         {
//             path: '',
//             redirectTo: 'HomeComponent',
//             pathMatch: 'full'
//           },
//         ]
//     }
// ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
